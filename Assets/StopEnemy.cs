using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopEnemy : MonoBehaviour
{
    
    public WallController wallController;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyController>()&&wallController.canBuild == false)
        {
            other.GetComponent<EnemyController>().navMeshAgent.enabled = false;
            wallController.OnHit(other.GetComponent<EnemyController>().hitValue);
        }
    }

}
