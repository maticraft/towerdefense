using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MeteorController : MonoBehaviour
{






    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private GameObject meteorParticles;
    [SerializeField] Camera camera;
    bool canMetorFly = false;
    public LayerMask terrainLayer;
    public Vector3 endPosition;
    

    private void Start()
    {
        camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            canMetorFly = true;
            var sequence = DOTween.Sequence();
            sequence.Append(transform.DOMove(endPosition, 0.5f));
            sequence.AppendCallback(() => OnTerrainCollisionEnter());
            
        
        
        }

        if (!canMetorFly)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, terrainLayer))
            {
                endPosition = hit.point ;
            }
   
        }
    
    }
    private void OnTerrainCollisionEnter()
    {

        meteorParticles.transform.SetParent(null);
        meteorParticles.SetActive(true);
        Destroy(meteorParticles, 5);
        var sequence = DOTween.Sequence();
        sequence.Append(transform.DOScale(Vector3.zero, 1.5f));
        sequence.Join(meshRenderer.material.DOFade(0, 1.5f)).OnComplete(() =>
        {
            Destroy(transform.gameObject);
        });

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyController>())
        {
            other.GetComponent<EnemyController>().OnHit(1000);
            
        }
    }
}

