using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class TowerController : MonoBehaviour
{
    [SerializeField] float refreshTime;
    [SerializeField] int damgeValue;
    [SerializeField] Camera camera;
    public LayerMask terrainLayer;
    bool canBuild = false;
    public List<EnemyController> enemyControllers = new List<EnemyController>();
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<EnemyController>())
        {
            enemyControllers.Add(other.GetComponent<EnemyController>());
        } 
    }
    private void Start()
    {
        canBuild = true;
        camera = Camera.main;
        StartCoroutine(scanForEnemy());
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            canBuild = false;
        }

        if (canBuild)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, terrainLayer))
            {
                transform.position = hit.point + new Vector3(0, 1, 0);
            }
        }
    }

    IEnumerator scanForEnemy()
    {
        yield return new WaitForSeconds(refreshTime);
        
        foreach(EnemyController enemyController in enemyControllers)
        {
            if (enemyController != null)
            {
                enemyController.OnHit(damgeValue);
                break;
            }

            }
            StartCoroutine(scanForEnemy());
    }
}
