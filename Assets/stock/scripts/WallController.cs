using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{

    public Transform startingPosition;
    [SerializeField] Camera camera;
    public LayerMask terrainLayer;

    public bool canBuild = false;
    public float rotationMultipler;
    public float hpWall;

    private void Start()
    {
        canBuild = true;
        camera = Camera.main;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0)&& CheckDistance() == true)
        {
            
            canBuild = false;
        }




        if (canBuild)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, terrainLayer))
            {
                transform.position = hit.point + new Vector3(0, 1, 0);

                transform.eulerAngles += new Vector3(0,1,0) * rotationMultipler * Input.GetAxis("Mouse ScrollWheel");


            }
        }
   }
    public bool CheckDistance()
    {
        if (Vector3.Distance(startingPosition.position, transform.position) < 10)
        {
            return false;
       
        }
        else
        {
            return true;
        }

    
    }
    public void OnHit(float value )
    {
        hpWall -= value;
        if (hpWall <= 0)
        {
            OnDie();
        }
    }
    public void OnDie()
    {
        Destroy(gameObject);
    }















}
