﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{
    public EnemiesController enemiesController;
    public NavMeshAgent navMeshAgent;
    public PlayerController playerController;
    public Vector3 destination;
    [SerializeField] private float distanceToDestintion;
    [SerializeField] public float hitValue;
    [SerializeField] TMP_Text hpText;
    [SerializeField] GameObject blood;
    [SerializeField] float timeToDestroyBlood;
    public int hpSoldier;

    public void Update()
    {
        if (Checkdistancetodestination())
        {
            playerController.HpDecrease(hitValue);
            enemiesController.enemies.Remove(this.gameObject.transform);
            
            Destroy(this.gameObject);
        }
    }


    public bool Checkdistancetodestination()
    {
        if (Vector3.Distance(transform.position, destination) < distanceToDestintion)
        {
            return true;

        }
        else
        {
            return false;
        }





    }
    public void OnHit(int value)
    {
        hpSoldier -= value;
        hpText.text = $"{hpSoldier}%";
        if (hpSoldier <= 0)
        {
            GameObject spawnedBlood = Instantiate(blood);
            spawnedBlood.transform.position = transform.position;
            enemiesController.enemies.Remove(this.transform);
            playerController.IncreseScore(1);
            Destroy(spawnedBlood, timeToDestroyBlood);
            Destroy(this.gameObject, 0.25f);
           


        }

    }

}


