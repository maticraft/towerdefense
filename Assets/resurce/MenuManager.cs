﻿
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
    [SerializeField] private RawImage logoImage;
    [SerializeField] private Color logoColor;
    [SerializeField] private float logoAnimateTime = 1.0f;
    [SerializeField] private AnimationCurve logoanimationCurve;
    [SerializeField] private Vector2 logoanimationemdpos;
    [SerializeField] private Vector3 logoanimationemdRotation;
    [SerializeField] private TMP_Text textAppVersion;
    [SerializeField] private Button buttonQuit;
    [SerializeField] private string firstLevelName;
    
    private void Start()
    {
        Animatelogo();
        CheckAppVersion();
    }

    private void Animatelogo()
    {
        logoImage.DOColor(logoColor, logoAnimateTime).SetEase(logoanimationCurve);
        logoImage.GetComponent<RectTransform>().DOAnchorPos(logoanimationemdpos, logoAnimateTime);
        logoImage.GetComponent<RectTransform>().DOLocalRotate(logoanimationemdRotation, logoAnimateTime);
    }

    private void CheckAppVersion()
    {
        textAppVersion.text = $"V.{Application.version}";
    }

    public void OnQuitButton()
    {
        Application.Quit();
    }

    public void OnSturtbutton()
    {
        SceneManager.LoadScene(firstLevelName);
    }
}    
