﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class EnemiesController : MonoBehaviour
{

    public List<Transform> enemies = new List<Transform>();
    public List<float> enemiesWavesValue = new List<float>();
    [SerializeField] float spawnTime = 1.0f;
    [SerializeField] public GameObject enemy;
    [SerializeField] public Transform destination;
    [SerializeField] public Transform startposition;
    [SerializeField] float enemiesSpeed = 10.0f;
    [SerializeField] private float hitValue;
    [SerializeField] PlayerController playerController;















    void Start()
    {
        StartCoroutine(spawningenemis());
    }
    public int currentEnemyIndex;
    public int currentWave;
    IEnumerator spawningenemis()
    {
        yield return new WaitForSeconds(spawnTime);
        EnemyController spawnedEnemy = Instantiate(enemy.GetComponent<EnemyController>());
        spawnedEnemy.transform.position = startposition.position;
        
        spawnedEnemy.destination = destination.position;
        spawnedEnemy.enemiesController = this;
        spawnedEnemy.hitValue = hitValue;
        spawnedEnemy.playerController = playerController;



        yield return new WaitForSeconds(0.1f);
        NavMeshAgent navMeshAgent = spawnedEnemy.navMeshAgent;
        if (navMeshAgent.enabled == true)
        {
            navMeshAgent.SetDestination(destination.position);
        }


            navMeshAgent.speed = enemiesSpeed;
        
        enemies.Add(spawnedEnemy.transform);
        currentEnemyIndex++;

        if (currentEnemyIndex < enemiesWavesValue[currentWave])
        {
            StartCoroutine(spawningenemis());
        }
        else
        {
            yield return new WaitUntil(() => enemies.Count == 0);
            currentEnemyIndex = 0;
            currentWave++;
            StartCoroutine(spawningenemis());
        }
       


    }
}
