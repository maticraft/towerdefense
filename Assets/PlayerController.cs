using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class PlayerController : MonoBehaviour
{
    [SerializeField] TMP_Text scoreText;
    [SerializeField] private Image hpBarBunker;
    [SerializeField] float maxHp;
    [SerializeField] float currentHp;
    [SerializeField] Vector3 meteorStartPosition;
    [SerializeField] ParticleSystem smoke;
    [SerializeField] GameObject tower1;
    [SerializeField] GameObject tower2;
    [SerializeField] GameObject spell1;

    [SerializeField] Transform startingPosition;
    public int currentScore;

    private void Start()
    {
        IncreseScore(10);
    }




    public void HpDecrease(float hitValue)
    {
        currentHp -= hitValue;
        hpBarBunker.fillAmount = currentHp;

        var emission = smoke.emission;
        if (currentHp <= 0.3f)
        {
            OnDeath();
        }
        if (currentHp <= 0.5f)
        {
            emission.rateOverTime = 299.99f;
        }
        if (currentHp <= 0.8f && currentHp >= 0.8f)
        {
            emission.rateOverTime = 5;
            smoke.gameObject.SetActive(true);

        }
    }
    public void OnTower1Click()
    {
        if (currentScore - 5 >= 0)
        {
            GameObject spawnedTower = Instantiate(tower1);
            spawnedTower.transform.position = Vector3.zero;
            Decrase(5);
        }

    }

    public void OnWall1Click()
    {
        if (currentScore - 3 >= 0)
        {
            GameObject spawnedWall = Instantiate(tower2);
            spawnedWall.transform.position = Vector3.zero;
            spawnedWall.GetComponent<WallController>().startingPosition = startingPosition;
            Decrase(3);
        }
        
    }

    public void OnSpell1Click()
    {
        if (currentScore - 10 >= 0)
        {
            GameObject spawnedSpell = Instantiate(spell1);
            spawnedSpell.transform.position = meteorStartPosition;
            Decrase(10);
        }


    }


    private void OnDeath()
    {
        SceneManager.LoadScene("mainmenu");
    }
    public void Decrase(int amount)
    {
        currentScore -= amount;
        scoreText.text = currentScore.ToString();
    }

    public void IncreseScore(int amount)
    {
        currentScore += amount;
        scoreText.text = currentScore.ToString();
    }
}
